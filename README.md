Approach:

First of all I read a bit about EmberJS on the site to know what it is all about. After that I downloaded the starter kit and started to do some tutorials from Code Logical to get to known to EmberJS. In the tutorials I learned some methods which can be used by the application.
Then I began to build the application. I started with creating the views and routes. For this I used the knowledge I acquired from the tutorials.  When this was finished, I searched the web (mainly the EmberJS documentation and StackOverflow) for the functions I wanted to use and how to implement them with the controllers.
While programming I observed the WebApp in the browser using an ember plugin to see e.g. my models are created and stored. When errors occurred, I used the console output of the browser to solve them.
In the end I did some manually testing by entering strings and watching what happens in the App using the ember plugin.
