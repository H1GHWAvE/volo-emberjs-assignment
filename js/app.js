App = Ember.Application.create();

App.Router.map(function() {
  this.route('json', {path: 'json/:person_id'});
});

App.ApplicationAdapter = DS.FixtureAdapter.extend({});

App.Person = DS.Model.extend({
  name: DS.attr('string'),
  email: DS.attr('string')
});

App.Person.FIXTURES = [];

App.IndexController = Ember.ObjectController.extend({
  actions: {
    validate: function() {
      var regex = /.+@.+\...+/,
          name = this.get('model.name'),
          email = this.get('model.email');
      if (regex.test(email)) {
        var person = this.store.createRecord(App.Person, {
                      name: name,
                      email: email
        });
        person.save();
        this.transitionToRoute('json', person.id.split("-")[1]);
      }
      else
        this.transitionToRoute('error', {message: 'The entered email address was not valid'});
      }
    }
});

App.JsonController = Ember.ObjectController.extend({
  actions: {
    back: function() {
      this.transitionToRoute('index');
    }
  }
});

App.ErrorController = Ember.ObjectController.extend({
  actions: {
    back: function() {
      this.transitionToRoute('index');
    }
  }
});

App.IndexModel = Ember.Object.extend({
  name: "",
  email: ""
});

App.IndexRoute = Ember.Route.extend({
  model: function() {
    return App.IndexModel.create();
  }
});

App.JsonRoute = Ember.Route.extend({
  model: function(params) {
    var person = this.store.find('person', "fixture-"+params.person_id).then(function(person) {person.json = JSON.stringify(person);
      return person;
    });
    return person;
  }
});
